package com.utest.testcases;

import java.awt.AWTException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.utest.pages.Articles;
import com.utest.pages.LoginPage;
import com.utest.pages.SigninPage;
import com.utest.utility.BrowserFactory;


public class SignupSignin {
	WebDriver driver;
	
	@BeforeMethod
	public void Browseropen() throws InterruptedException

	{
	    driver=BrowserFactory.startBrowser("chrome", "https://www.utest.com");
	    Thread.sleep(8000);
	}
	
	@AfterMethod
	public void Browserclose()
	{
	
		driver.close();
	}
	//valid signin
	@Test(priority=1,enabled=true)
	public void signupsuccess() throws InterruptedException, FindFailed
	{
		//This will launch browser and specific url
				//WebDriver driver=BrowserFactory.startBrowser("chrome", "https://www.utest.com");
				
				SigninPage signin=PageFactory.initElements(driver, SigninPage.class);
				
				signin.utestsignin("Nepal", "Senapati", "nepal344@gmail.com", "october", "23", "1986", "Male","Sadhan.343!","Sadhan.343!");
				
				Assert.assertEquals(signin.Validsignin(), true);
				//driver.close();	
	}
	
	//invalid signin
	@Test(priority=2,enabled=true)
	public void signupfail() throws FindFailed, InterruptedException
	{
		SigninPage signin1=PageFactory.initElements(driver, SigninPage.class);
		
		signin1.InvalidUtestsignin("Nepal", "Senapati", "nepal343@gmail.com", "october", "23", "1986", "Male");
		Assert.assertEquals(signin1.InValidsignin(),true);
	}
	
	
	
	
	//valid login
	@Test(priority=3,enabled=true)
	public void loginsuccess() throws InterruptedException
	{
		//WebDriver driver=BrowserFactory.startBrowser("chrome", "https://www.utest.com");
		LoginPage login=PageFactory.initElements(driver, LoginPage.class);
		login.utestlogin("nepal343@gmail.com","Sadhan.343!");
		Assert.assertEquals(login.verifyValidLogin(), true);
		//driver.close();
	}
	
	//invalid login
	@Test(priority=4,enabled=true)
	public void loginfail() throws InterruptedException
	{
		//WebDriver driver=BrowserFactory.startBrowser("chrome", "https://www.utest.com");
		LoginPage login=PageFactory.initElements(driver, LoginPage.class);
		login.utestlogin("nepal343@gmail.com","Sadhan.3433!");
		
		Assert.assertEquals(login.verifyInValidLogin(), true);
		//driver.close();
	}
	
	@Test(priority=5,enabled=true)
	public void savearticles() throws InterruptedException, AWTException
	{
		Articles art=PageFactory.initElements(driver, Articles.class);
		art.utestloginpro("nepal343@gmail.com", "Sadhan.343!","nepal senapati11");
		
		Assert.assertEquals(art.Verifysavee(), true);
	}

}
