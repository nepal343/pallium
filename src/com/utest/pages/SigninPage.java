package com.utest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class SigninPage {
	
	WebDriver driver;
	
	public SigninPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	@FindBy(xpath="/html[1]/body[1]/span[1]/div[3]/div[1]/div[4]/div[1]/div[1]/a[2]")
	@CacheLookup
	WebElement signup;
	
	@FindBy(xpath="//input[@name='firstName']")
	@CacheLookup
	WebElement firstname;
	
	@FindBy(name="lastName")
	@CacheLookup
	WebElement lastname;
	
	@FindBy(name="email")
	@CacheLookup
	WebElement email;
	
	@FindBy(name="birthMonth")
	@CacheLookup
	WebElement dobmonth;

	@FindBy(name="birthDay")
	@CacheLookup
	WebElement dobday;
	
	@FindBy(name="birthYear")
	@CacheLookup
	WebElement dobyear;
	
	@FindBy(name="genderCode")
	@CacheLookup
	WebElement gender;
	
	@FindBy(name="languages")
	@CacheLookup
	WebElement language;
	
	@FindBy(xpath="//span[contains(text(),'Next: Location')]")
	@CacheLookup
	WebElement next;
	
	@FindBy(name="password")
	@CacheLookup
	WebElement pass;
	
	@FindBy(name="confirmPassword")
	@CacheLookup
	WebElement confirmpass;
	
	@FindBy(xpath="/html[1]/body[1]/span[1]/main[1]/section[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[4]/label[1]/span[1]")
	@CacheLookup
	WebElement term1;
	
	@FindBy(xpath="/html[1]/body[1]/span[1]/main[1]/section[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[5]/label[1]/span[1]")
	@CacheLookup
	WebElement term2;
	
	@FindBy(xpath="/html[1]/body[1]/span[1]/main[1]/section[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[6]/label[1]/span[1]")
	@CacheLookup
	WebElement term3;
	
	@FindBy(xpath="/html[1]/body[1]/span[1]/div[3]/div[1]/div[2]/div[1]/div[2]/span[1]/a[1]")
	@CacheLookup
	WebElement accept;
	
	@FindBy(xpath="//span[contains(text(),'Email already registered')]")
	@CacheLookup
	WebElement errmsg;
	
	
	
	public void utestsignin(String fname,String lname,String mail,String month,String day,String year,String gndr,String pass1,String confirmpass1) throws InterruptedException, FindFailed
	{
		accept.click();
		signup.click();
		Thread.sleep(8000);
		firstname.sendKeys(fname);
		lastname.sendKeys(lname);
		email.sendKeys(mail);
		
		dobmonth.click();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		
		dobday.click();
		Actions act1 = new Actions(driver);
		 act1.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		
		//Select s2=new Select(dobday);
		//s2.selectByVisibleText(day);
		
		 dobyear.click();
		 Actions act2 = new Actions(driver);
		 act2.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
		 .sendKeys(Keys.ENTER).perform();
		//Select s3=new Select(dobyear);
		//s3.selectByVisibleText(year);
		
		 
		 gender.click();
		 Actions act3 = new Actions(driver);
		 act3.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
			
		//Select s4=new Select(gender);
		//s4.deselectByVisibleText(gndr);
		
		//Select s5=new Select(language);
		//s5.selectByVisibleText(lang);
		
		 Screen screen=new Screen();
		 Pattern next1=new Pattern("./image/next1.png");
		 Pattern next2=new Pattern("./image/next2.png");
		 Pattern scroll=new Pattern("./image/scroll.png");
		 Pattern next3=new Pattern("./image/next3.png");
		 Pattern comp1=new Pattern("./image/comp.png");
		 
		 screen.click(scroll);
		 Thread.sleep(1000);
		 screen.click(next1);
		 Thread.sleep(1000);
		 screen.click(scroll);
		 Thread.sleep(1000);
		 screen.click(next2);
		 Thread.sleep(1000);
		 screen.click(next3);
		 Thread.sleep(1000);
		 pass.sendKeys(pass1);
		 confirmpass.sendKeys(confirmpass1);
		 term1.click();
		 term2.click();
		 term3.click();
		
		 Actions act4=new Actions(driver);
		 act4.sendKeys(Keys.PAGE_DOWN).build().perform();
		 Thread.sleep(1000);
		 screen.click(comp1);
		 Thread.sleep(5000);
		 
	}
	
	
	//InValidSignin
		public void InvalidUtestsignin(String fname,String lname,String mail,String month,String day,String year,String gndr) throws InterruptedException, FindFailed 
		{
			accept.click();
			signup.click();
			Thread.sleep(2000);
			firstname.sendKeys(fname);
			lastname.sendKeys(lname);
			email.sendKeys(mail);
			
			dobmonth.click();
			Thread.sleep(1000);
			Actions act = new Actions(driver);
			Thread.sleep(1000);
			act.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
			
			dobday.click();
			Thread.sleep(1000);
			Actions act1 = new Actions(driver);
			 act1.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
			
			//Select s2=new Select(dobday);
			//s2.selectByVisibleText(day);
			
			 dobyear.click();
			 Thread.sleep(1000);
			 Actions act2 = new Actions(driver);
			 act2.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
			 .sendKeys(Keys.ENTER).perform();
			//Select s3=new Select(dobyear);
			//s3.selectByVisibleText(year);
			
			 
			 gender.click();
			 Thread.sleep(1000);
			 Actions act3 = new Actions(driver);
			 act3.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
				
			//Select s4=new Select(gender);
			//s4.deselectByVisibleText(gndr);
			
			//Select s5=new Select(language);
			//s5.selectByVisibleText(lang);
			
			 Screen screen=new Screen();
			 Pattern next1=new Pattern("./image/next1.png");
			 Pattern scroll=new Pattern("./image/scroll.png");
			 
			 screen.click(scroll);
			 Thread.sleep(1000);
			 screen.click(next1);
		}
	
	public boolean Validsignin()
	{
		if
		(driver.getTitle().equalsIgnoreCase("Software Testing - uTest"))
		{
			return true;
		}
		return false;
		
	}
	
	public boolean InValidsignin()
	{
		if
		(errmsg.getText().equalsIgnoreCase("Email already registered"))
		{
			return true;
		}
		return false;
		
	}
	;
	

}
