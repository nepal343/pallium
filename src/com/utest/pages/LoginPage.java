package com.utest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class LoginPage {
	//testing bigbucket
	WebDriver driver;
	
	public LoginPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	@FindBy(xpath="//a[contains(text(),'Sign in')]")
	@CacheLookup
	WebElement signin;
	
	@FindBy(name="username")
	@CacheLookup
	WebElement uid;
	
	@FindBy(name="password")
	@CacheLookup
	WebElement pass;
	
	@FindBy(xpath="//button[@type='submit']")
	@CacheLookup
	WebElement submit;
	
	@FindBy(xpath="/html[1]/body[1]/span[1]/div[3]/div[1]/div[2]/div[1]/div[2]/span[1]/a[1]")
	@CacheLookup
	WebElement accept;
	
	@FindBy(xpath="//label[contains(text(),'Wrong credentials')]")
	@CacheLookup
	WebElement errmsg;
	
	@FindBy(xpath="//h2[contains(text(),'Welcome Back, Nepal Senapati!')]")
	@CacheLookup
	WebElement sucmsg;
	
	public void utestlogin(String uname,String pwd) throws InterruptedException
	{
		accept.click();
		signin.click();
		Thread.sleep(5000);
		uid.sendKeys(uname);
		pass.sendKeys(pwd);
		submit.click();
		Thread.sleep(5000);
	}
	public boolean verifyValidLogin()
	{
		System.out.println(sucmsg.getText());
		if
		(sucmsg.getText().equalsIgnoreCase("Welcome Back, Nepal Senapati!"))
		{
			return true;
		}
		return false;			
	}
	public boolean verifyInValidLogin()
	{
		System.out.println(errmsg.getText());
		if
		(errmsg.getText().equalsIgnoreCase("Wrong credentials"))
		{
			return true;
		}
		return false;			
	}

	
	
	
	

}
