package com.utest.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class Articles {
	
	
WebDriver driver;
	
	public Articles(WebDriver driver)
	{
		this.driver=driver;
	}
	
	@FindBy(xpath="//a[contains(text(),'Sign in')]")
	@CacheLookup
	WebElement signin;
	
	@FindBy(name="username")
	@CacheLookup
	WebElement uid;
	
	@FindBy(name="password")
	@CacheLookup
	WebElement pass;
	
	@FindBy(xpath="//button[@type='submit']")
	@CacheLookup
	WebElement submit;
	
	@FindBy(xpath="/html[1]/body[1]/span[1]/div[3]/div[1]/div[2]/div[1]/div[2]/span[1]/a[1]")
	@CacheLookup
	WebElement accept;
	
	@FindBy(xpath="/html[1]/body[1]/span[1]/div[1]/div[1]/div[2]/div[2]/ul[1]/li[1]/a[1]")
	@CacheLookup
	WebElement articles;
	
	@FindBy(xpath="/html[1]/body[1]/span[1]/div[3]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/a[1]")
	@CacheLookup
	WebElement editarticle;
	
	@FindBy(xpath="//input[@placeholder='Article Title']")
	@CacheLookup
	WebElement arttitle;
	
	@FindBy(xpath="//*[@id=\"mainContent\"]/div[2]/div[3]/div/div[2]/div[6]/div[1]/div/div/div/div[5]/pre")
	@CacheLookup
	WebElement artbody;
	
	@FindBy(xpath="//a[@class='btn btn-save editor-footer-action-btn']")
	@CacheLookup
	WebElement save;
	
	//@FindBy(xpath="//span[contains(text(),'Auto save completed')]")
	@FindBy(xpath="//small[@class='text-success']")
	@CacheLookup
	WebElement verifysave;
	
	
	
	public void utestloginpro(String uname,String pwd,String title) throws InterruptedException, AWTException
	{
		accept.click();
		signin.click();
		Thread.sleep(5000);
		uid.sendKeys(uname);
		pass.sendKeys(pwd);
		submit.click();
		Thread.sleep(5000);
		articles.click();
		Thread.sleep(5000);
		editarticle.click();
		arttitle.sendKeys(title);
		Thread.sleep(5000);
		Robot rob=new Robot();
		rob.keyPress(KeyEvent.VK_TAB);
		rob.keyPress(KeyEvent.VK_W);
		rob.keyPress(KeyEvent.VK_E);
		rob.keyPress(KeyEvent.VK_L);
		rob.keyPress(KeyEvent.VK_C);
		rob.keyPress(KeyEvent.VK_O);
		rob.keyPress(KeyEvent.VK_M);
		rob.keyPress(KeyEvent.VK_E);
		save.click();
		Actions act4 = new Actions(driver);
		act4.sendKeys(Keys.PAGE_UP).build().perform();
		
		Thread.sleep(5000); 
		
		

		//artbody.sendKeys("test body");
		
		
	}
	
	public boolean Verifysavee()
	{
		System.out.println(verifysave.getText());
		if
		(verifysave.getText().equalsIgnoreCase("Auto save completed"))
		{
			return true;
		}
		return false;
		
	}
	
	
	

}
